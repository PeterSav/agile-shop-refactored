import React from "react";

const Wording = ({ message, style }) => {
    return <p style={style}>{message}</p>;
};

export default Wording;
