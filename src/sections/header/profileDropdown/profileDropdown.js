import Wording from "../../commonComponents/wording/wording";
import React from "react";
import "./profileDropdown.scss";

const ProfileDropdown = () => {
    return (
        <span className="header__profile">
            <i className="fas fa-user-alt" />
            <div className="header__cart__profile__dropDownMenu moveLeft">
                <Wording
                    message="Welcome Guest"
                    style={{
                        fontSize: "24px",
                        marginTop: "1.5em",
                        fontWeight: "bold"
                    }}
                />
                <button className="header__profile__signButton borderNone">
                    Sign in
                </button>
                <button className="header__profile__profileInfo borderNone">
                    Profile info
                </button>
            </div>
        </span>
    );
};

export default ProfileDropdown;
