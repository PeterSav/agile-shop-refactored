import "./header.scss";
import React from "react";
import logo from "../../imgs/logo.png";
import NavButton from "./navButton/navButton";
import navButtonsToRender from "./navButton/navButtonsToRender";
import CartDropdown from "./cartDropdown/cartDropdown";
import ProfileDropdown from "./profileDropdown/profileDropdown";

const Header = () => {
    const createNavButtons = navButton => {
        return (
            <NavButton
                title={navButton.title}
                styleClass={navButton.styleClass}
            />
        );
    };

    return (
        <div className="header__colorizer">
            <header className="header__flex">
                <a href="#header">
                    <img src={logo} alt="store logo" />
                </a>
                {navButtonsToRender.map(navButton =>
                    createNavButtons(navButton)
                )}
                <CartDropdown />
                <ProfileDropdown />
                <div className="hidden">hello</div>
            </header>
        </div>
    );
};

export default Header;
