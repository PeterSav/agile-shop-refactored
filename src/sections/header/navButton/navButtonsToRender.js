const navButtonsToRender = [
    {
        title: "New releases",
        styleClass: "header__navButton"
    },
    {
        title: "sales",
        styleClass: "header__navButton"
    },
    {
        title: "categories",
        styleClass: "header__navButton"
    }
];

export default navButtonsToRender;
