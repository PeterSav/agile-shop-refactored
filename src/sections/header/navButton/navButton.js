import "./navButton.scss";
import React from "react";

const NavButton = ({ title, styleClass }) => {
    return <nav className={styleClass}>{title}</nav>;
};

export default NavButton;
