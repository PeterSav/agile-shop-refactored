import "./cartDropdown.scss";
import React from "react";
import Wording from "../../commonComponents/wording/wording";

const CartDropdown = () => {
    return (
        <span className="header__cart">
            <i className="fas fa-shopping-cart" />
            <span className="header__cart__items">10</span>
            <div className="header__cart__profile__dropDownMenu">
                <Wording
                    message="your cart has 10 items"
                    style={{
                        textAlign: "center",
                        fontSize: "1.25em"
                    }}
                />
                <Wording
                    message="Cost: 1299$"
                    style={{
                        textAlign: "center",
                        fontSize: "1.75em",
                        fontWeight: "bold"
                    }}
                />
                <button className="header__cart__checkoutButton borderNone">
                    Checkout
                </button>
                <button className="header__cart__empty borderNone">
                    Empty cart
                </button>
            </div>
        </span>
    );
};

export default CartDropdown;
